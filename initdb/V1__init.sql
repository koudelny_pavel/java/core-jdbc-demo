CREATE TABLE public.books (
    id bigint NOT NULL GENERATED ALWAYS AS IDENTITY,
    book_name varchar(100) NOT NULL,
    author varchar(100) NULL,
    CONSTRAINT book_pkey PRIMARY KEY (id)
);

insert into public.books (book_name, author)
    values ('Мастер и Мергарита', 'Булгаков');

insert into public.books (book_name, author)
    values ('Му-Му', 'Тургенев');

insert into public.books (book_name, author)
    values ('Тихий Дон', 'Шолохов');

insert into public.books (book_name, author)
    values ('Горе от ума', 'Грибоедов');
