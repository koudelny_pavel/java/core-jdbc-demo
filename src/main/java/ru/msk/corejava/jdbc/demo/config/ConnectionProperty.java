package ru.msk.corejava.jdbc.demo.config;

import lombok.Data;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Data
public class ConnectionProperty {
    private String dbUrl;
    private String dbUserName;
    private String dbPassword;



    public ConnectionProperty(String resourceName){
        try (InputStream input = ConnectionProperty.class.getClassLoader().getResourceAsStream(resourceName)) {
            if (input == null) {
                throw new RuntimeException("Unable to find "+resourceName);
            }
            Properties prop = new Properties();
            prop.load(input);
            dbUrl = prop.getProperty("db.url");
            dbUserName = prop.getProperty("db.user_name");
            dbPassword = prop.getProperty("db.password");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
