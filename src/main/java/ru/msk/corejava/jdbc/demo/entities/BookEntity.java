package ru.msk.corejava.jdbc.demo.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BookEntity {
    private Long id;
    private String bookName;
    private String author;

    @Override
    public String toString() {
        return String.format("%s / %s (%d)", bookName, author, id);
    }
}
