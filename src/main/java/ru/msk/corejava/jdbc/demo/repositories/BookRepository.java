package ru.msk.corejava.jdbc.demo.repositories;

import ru.msk.corejava.jdbc.demo.entities.BookEntity;
import java.util.Collection;

// TODO Подумать над применением типа Optional<BookEntity>
public interface BookRepository {
    Collection<BookEntity> getAll();
    // Странное название для метода, который делает insert записи в БД
    boolean save(BookEntity book);
    boolean delete(Long bookId);
}
