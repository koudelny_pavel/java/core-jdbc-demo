package ru.msk.corejava.jdbc.demo.services;

import ru.msk.corejava.jdbc.demo.entities.BookEntity;
import ru.msk.corejava.jdbc.demo.models.Book;
import ru.msk.corejava.jdbc.demo.repositories.BookRepository;

public class BookService {
    private BookRepository bookRepository;

    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    void printAll(){
        bookRepository.getAll().forEach(System.out::println);
// Вариант без Stream API
//        for (BookEntity bookEntity: bookRepository.getAll()) {
//            System.out.println(bookEntity);
//        }
    }

    public void save(Book book){
        bookRepository.save(
                new BookEntity(null, book.getBookName(), book.getAuthor())
        );
    }

    // TODO Не правильно передавать id в параметрах, нужно передавать Book
    public void delete(Long bookId){
        bookRepository.delete(bookId);
    }
}
