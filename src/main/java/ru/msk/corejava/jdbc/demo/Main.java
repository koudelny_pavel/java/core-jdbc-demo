package ru.msk.corejava.jdbc.demo;

import ru.msk.corejava.jdbc.demo.entities.BookEntity;
import ru.msk.corejava.jdbc.demo.repositories.BookRepository;
import ru.msk.corejava.jdbc.demo.repositories.BookRepositoryImpl;

import java.sql.Connection;
import java.util.Collection;

public class Main {
    // TODO: нужно написать Unit-тесты с использованием БД H2
    // TODO Сделать интерфейс BookService, а этот класс переименовать в BookServiceImpl
    // TODO: Как правильно закрывать connection вызовом метода close() в BookRepositoryImpl?
    public static void main(String[] args) {
        BookRepository bookRepository = new BookRepositoryImpl();
        Collection<BookEntity> books = bookRepository.getAll();
        System.out.println(books);
    }
}