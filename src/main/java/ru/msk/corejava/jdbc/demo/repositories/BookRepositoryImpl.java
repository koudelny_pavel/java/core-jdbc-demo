package ru.msk.corejava.jdbc.demo.repositories;

import ru.msk.corejava.jdbc.demo.config.ConnectionProperty;
import ru.msk.corejava.jdbc.demo.entities.BookEntity;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class BookRepositoryImpl implements BookRepository{
    private Connection connection;
    private ConnectionProperty prop;

    public BookRepositoryImpl() {
        prop = new ConnectionProperty("connection.property");
        try {
            connection = DriverManager.getConnection(
                    prop.getDbUrl(),
                    prop.getDbUserName(),
                    prop.getDbPassword());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Collection<BookEntity> getAll() {
        List<BookEntity> result = new ArrayList<>();
        String sql = "SELECT * FROM public.books;";
        try (Statement statement = connection.createStatement()){
            final ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()){
                // Или лучше сначала в промежуточные переменные читать?
                BookEntity book = new BookEntity(
                        resultSet.getLong("id"),
                        resultSet.getString("book_name"),
                        resultSet.getString("author")
                );
                result.add(book);
            }
        } catch (SQLException e) {
            //throw new RuntimeException(e);
            e.printStackTrace();
            return null;
        }
        return result;
    }

    @Override
    public boolean save(BookEntity book) {
        String sql = "INSERT INTO public.books(book_name, author) values(?, ?);";
        try(final PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)){
            // Номера параметров начинаются не с нуля, а с единицы !!!
            statement.setString(1, book.getBookName());
            statement.setString(2, book.getAuthor());
            final int rowCount = statement.executeUpdate();
            final ResultSet generatedKeys = statement.getGeneratedKeys();
            generatedKeys.next();
            Long id = generatedKeys.getLong(1);
            book.setId(id);
            return rowCount == 1;
        } catch (SQLException e) {
            //throw new RuntimeException(e);
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean delete(Long bookId) {
        String sql = "DELETE FROM public.books where id = ?;";
        try(final PreparedStatement statement = connection.prepareStatement(sql)){
            statement.setLong(1, bookId);
            return statement.executeUpdate() == 1;
        } catch (SQLException e) {
            //throw new RuntimeException(e);
            e.printStackTrace();
            return false;
        }
    }
}
